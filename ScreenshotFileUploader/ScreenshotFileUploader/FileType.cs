﻿namespace ScreenshotFileUploader
{
    public enum FileType
    {
        png,
        txt,
        zip
    }
}
