﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Ionic.Zip;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;

namespace ScreenshotFileUploader
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        private readonly IDistributedCache _distributedCache;
        public FilesController(
            IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }

        // GET api/values
        [HttpGet("{id}")]
        public ActionResult Get(Guid id)
        {
            var directoryPath = FileHelper.GetDirectoryPath(id);
            if (!Directory.Exists(directoryPath))
            {
                return NotFound();
            }

            using (ZipFile zip = new ZipFile())
            {
                var files = Directory.GetFiles(directoryPath);
                zip.AddFiles(files, false, string.Empty);
                using (var output = new MemoryStream())
                {
                    zip.Save(output);

                    return File(output.ToArray(), "application/zip", 
                        FileHelper.GenerateNewFileName(FileType.zip));
                }
            }
        }

        [HttpPost("images")]
        public async Task UploadImage([FromBody] UploadImageRequest request)
        {
            Directory.CreateDirectory(FileHelper.GetDirectoryPath(request.Id));
            using (var ms = new MemoryStream(request.Image))
            {
                var image = Image.FromStream(ms);
                image.Save(FileHelper.GetDirectoryChildPath(
                    request.Id, FileHelper.GenerateNewFileName(FileType.png)));
            }

            await UpdateCache(request.Id);
        }

        [HttpPost("textfiles")]
        public async Task WriteTextToFile([FromBody] WriteToFileRequest request)
        {
            var directoryPath = FileHelper.GetDirectoryPath(request.Id);
            Directory.CreateDirectory(directoryPath);
            using (StreamWriter writer = System.IO.File.AppendText(
                $"{directoryPath}{Path.DirectorySeparatorChar}{request.Id.ToString()}"))
            {
                writer.WriteLine(request.Text);
            }

            await UpdateCache(request.Id);
        }

        private HttpResponseMessage ZipContentResult(ZipFile zipFile)
        {
            var pushStreamContent = new PushStreamContent((stream, content, context) =>
            {
                zipFile.Save(stream);
                stream.Close();
            }, "application/zip");

            return new HttpResponseMessage(HttpStatusCode.OK) { Content = pushStreamContent };
        }

        private async Task UpdateCache(Guid id)
        {
            var valueFromCache = await _distributedCache.GetStringAsync(id.ToString());
            if (valueFromCache == null)
            {
                // if cache miss, ignore, this service can only update cache
                return;
            }

            var cacheItem = valueFromCache.Deserialize();

            // update the number of files that have been processed
            cacheItem.IncrementFilesProcessed();

            var options = new DistributedCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromMinutes(20));

            await _distributedCache.SetStringAsync(
                    id.ToString(),
                    cacheItem.Serialize(),
                    options);
        }
    }
}
