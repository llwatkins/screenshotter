﻿namespace ScreenshotFileUploader
{
    public static class EnvironmentVariables
    {
        public const string RedisInstanceName = nameof(RedisInstanceName);
        public const string RedisConfiguration = nameof(RedisConfiguration);
    }
}
