﻿using System;
using System.IO;

namespace ScreenshotFileUploader
{
    public static class FileHelper
    {
        public static string GenerateNewFileName(FileType fileType)
            => $"{Guid.NewGuid()}.{fileType.ToString()}";

        public static string GetDirectoryPath(Guid id)
            => $"{Constants.Root}{Path.DirectorySeparatorChar}{id}";

        public static string GetDirectoryChildPath(Guid dir, string child)
            => $"{Constants.Root}{Path.DirectorySeparatorChar}{dir}{Path.DirectorySeparatorChar}{child}";
    }
}
