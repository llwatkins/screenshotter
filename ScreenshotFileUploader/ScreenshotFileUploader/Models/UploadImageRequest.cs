﻿using System;

namespace ScreenshotFileUploader
{
    public class UploadImageRequest
    {
        public Guid Id { get; set; }
        public byte[] Image { get; set; }
    }
}
