﻿using System;

namespace ScreenshotFileUploader
{
    public class WriteToFileRequest
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
    }
}
