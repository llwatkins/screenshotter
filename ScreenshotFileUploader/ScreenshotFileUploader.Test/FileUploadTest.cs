﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace ScreenshotFileUploader.Test
{
    public class FileUploadTest
    {
        private FilesController _subjectUnderTest;
        private TestDistributedCache _distributedCache = new TestDistributedCache();

        public FileUploadTest()
        {
            _subjectUnderTest = new FilesController(
                _distributedCache);
        }

        [Fact(DisplayName = "It should return not found when requesting for zip that does not exist")]
        public void ItShouldReturnNotFound()
        {
            var id = Guid.NewGuid();
            var response = _subjectUnderTest.Get(id) as StatusCodeResult;

            response.StatusCode.Should().Be((int)HttpStatusCode.NotFound);
        }

        [Fact(DisplayName = "It should create a new folder for an image")]
        public async Task ItShouldCreateANewFolderForImage()
        {
            var id = Guid.NewGuid();
            var expectedFolder = FileHelper.GetDirectoryPath(id);
            _distributedCache.SetCacheItem(new UploadStatusCacheItem(0, 0));

            var request = new UploadImageRequest
            {
                Id = id,
                Image = GetArrayFromTestImage()
            };

            await _subjectUnderTest.UploadImage(request);

            Directory.Exists(expectedFolder).Should().BeTrue();
            Directory.GetFiles(expectedFolder).Should().HaveCount(1);
            CleanUp(expectedFolder);
        }

        [Fact(DisplayName = "It should create a new folder for a text file")]
        public async Task ItShouldCreateANewFolderForTextFile()
        {
            var id = Guid.NewGuid();
            var expectedFolder = FileHelper.GetDirectoryPath(id);
            _distributedCache.SetCacheItem(new UploadStatusCacheItem(0, 0));

            var request = new WriteToFileRequest
            {
                Id = id,
                Text = "This is some text"
            };

            await _subjectUnderTest.WriteTextToFile(request);

            Directory.Exists(expectedFolder).Should().BeTrue();
            Directory.GetFiles(expectedFolder).Should().HaveCount(1);
            CleanUp(expectedFolder);
        }

        private byte[] GetArrayFromTestImage()
        {
            byte[] imageData = null;
            var imageLocation = $"{Directory.GetCurrentDirectory()}{Path.DirectorySeparatorChar}test.png";
            FileInfo fileInfo = new FileInfo(imageLocation);
            long imageFileLength = fileInfo.Length;
            FileStream fs = new FileStream(imageLocation, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            imageData = br.ReadBytes((int)imageFileLength);
            return imageData;
        }

        private void CleanUp(string folder)
        {
            DirectoryInfo di = new DirectoryInfo(folder);
            foreach (var file in di.GetFiles())
            {
                file.Delete();
            }

            Directory.Delete(folder);
        }
    }
}
