﻿using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScreenshotFileUploader.Test
{
    public class TestDistributedCache : IDistributedCache
    {
        private UploadStatusCacheItem _cacheItem 
            = new UploadStatusCacheItem(default(decimal), default(decimal));

        public void SetCacheItem(UploadStatusCacheItem cacheItem) 
            => _cacheItem = cacheItem;

        public byte[] Get(string key)
        {
            throw new NotImplementedException();
        }

        public Task<byte[]> GetAsync(string key, CancellationToken token = default(CancellationToken))
        {
            return Task.FromResult(Encoding.ASCII.GetBytes(_cacheItem.Serialize()));
        }

        public void Refresh(string key)
        {
            throw new NotImplementedException();
        }

        public Task RefreshAsync(string key, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public void Remove(string key)
        {
            throw new NotImplementedException();
        }

        public Task RemoveAsync(string key, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public void Set(string key, byte[] value, DistributedCacheEntryOptions options)
        {
        }

        public Task SetAsync(string key, byte[] value, DistributedCacheEntryOptions options, CancellationToken token = default(CancellationToken))
        {
            return Task.CompletedTask;
        }
    }
}
