using FluentAssertions;
using System;
using System.IO;
using Xunit;

namespace ScreenshotFileUploader.Test
{
    public class FileHelperTest
    {
        [InlineData(FileType.png)]
        [InlineData(FileType.txt)]
        [InlineData(FileType.zip)]
        [Theory(DisplayName = "It should generate a new file name")]
        public void ItShouldGenerateANewFileName(FileType fileType)
        {
            var fileName = FileHelper.GenerateNewFileName(fileType);
            var fileSplit = fileName.Split(".");

            fileSplit.Should().HaveCount(2);
            Guid.Parse(fileSplit[0]).Should().NotBe(default(Guid));
            fileSplit[1].Should().Be(fileType.ToString());
        }

        [Fact(DisplayName = "It should return a directory path")]
        public void ItShouldReturnADirectoryPath()
        {
            var id = Guid.NewGuid();
            var result = FileHelper.GetDirectoryPath(id);
            var pathSplit = result.Split(Path.DirectorySeparatorChar);

            pathSplit.Should().HaveCount(2);
            pathSplit[0].Should().Be(Constants.Root);
            pathSplit[1].Should().Be(id.ToString());
        }

        [Fact(DisplayName = "It should return a file path")]
        public void ItShouldReturnAFilePath()
        {
            var id = Guid.NewGuid();
            const string childFileName = "someName.txt";

            var result = FileHelper.GetDirectoryChildPath(id, childFileName);
            var pathSplit = result.Split(Path.DirectorySeparatorChar);

            pathSplit.Should().HaveCount(3);
            pathSplit[0].Should().Be(Constants.Root);
            pathSplit[1].Should().Be(id.ToString());
            pathSplit[2].Should().Be(childFileName);
        }
    }
}
