# Overview
For this project, I've decided to break this problem down into three separate deployables which are described below.
I decided to go with a web api as the main entry point due to its flexibility. It could be called by a console application
or serve as the backed end to a presenation layer. 

In order to run the project, simply navigate to the root directory where the docker-compose.yml lives and execute
docker-compose up.

### ScreenshotService
This service is a web api with three different operations:  
Send file with ';' delimited urls  
Send raw string with ';' delimited urls  
Retrieve file upload status  

This service is a Kafka producer that writes urls to the stream from a task that is enqueued/dequeued from a
task queue that runs in the background. It also utilizes a distributed redis cache for storing 
the processing status for each particular request.

Each unique request is associated with an id and is enqueued/dequeued from the backgound task queue separately.

#### Usage

For sending a raw ';' delimited string: 
![picture](rm_images/raw_string_post.png)

For sending a file with ';' delimited urls:
![picture](rm_images/file_post.png)

You will receive a guid id as the response for either request

With this guid, you can request the status of your upload:
![picture](rm_images/status_request.png)

Status request response:
![picture](rm_images/status_response.png)

#### Validations
I added validation for duplicate entries. If you would like to turn this off, navigate to the docker-compose.yml
at the root level of the directoy and edit the "AllowDuplicates" configuration value:
![picture](rm_images/dup_var.png)

**note: this does not mean that duplicates are actually processed, it just means that it will allow processing without
raising an error to the user. The urls are stored in a hash set so duplicates in the same request will never be streamed.**

### UrlToScreenshotProcessor
This is the Kafka consumer that listens to the stream for messages. When a message is received, it utilizes 
a headless chrome instance to capture a screenshot of the website. If the driver throws an exception, the processor
sends the error message to the file uploader and appends it to an error log file and continues processing. 
If the screenshot succeeds, the processor sends the captured byte array to file upload service.

### ScreenshotFileUploader
I generally favor storing files of this nature in cloud storage over a database, so I created this service to mimic
that scenario. This service simply receives a byte array and an associated id and saves them to its host file system.
It also zips up the files and serves them on request. The zip file may be requested at any time even if other messages
are still being streamed.

#### Usage

To request a zip file with the screenshots (**use the guid returned from the ScreenshotService above**):
![picture](rm_images/files_request.png)

Notice this service runs on port 5002 unlike the ScreenshotService which runs on 5000.

You'll receive this as a response:
![picture](rm_images/files_response.png)

You can also navigate to the url in a browser.

## Scalability
One way we could prepare for scale would be to
scale up the number of partitions and consumers. Since each message in the stream is associated
with a request id and the processors don't actually persist any data themselves, we don't need to worry about messages 
for a given request all being piped to the same partition, they can continue being round-robbin'd across all partions.

# Sweet Diagram
![picture](rm_images/sweet_diagram.png)