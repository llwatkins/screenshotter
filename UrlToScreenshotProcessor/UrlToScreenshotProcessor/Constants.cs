﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UrlToScreenshotProcessor
{
    public static class Constants
    {
        public const string ImageUploadEndpoint = "/api/files/images";
        public const string TextFileUploadEndpoint = "/api/files/textfiles";
    }
}
