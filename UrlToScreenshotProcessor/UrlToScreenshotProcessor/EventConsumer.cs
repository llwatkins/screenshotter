﻿using Confluent.Kafka;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading;

namespace UrlToScreenshotProcessor
{
    public class EventConsumer
    {
        private readonly Consumer<Null, string> _consumer;
        private readonly ChromeDriver _chromeDriver;
        private IConfiguration _configuration;
        private ILogger<EventConsumer> _logger;

        public EventConsumer(
            Consumer<Null, string> consumer,
            ChromeDriver chromeDriver,
            IConfiguration configuration,
            ILogger<EventConsumer> logger)
        {
            _consumer = consumer;
            _chromeDriver = chromeDriver;
            _configuration = configuration;
            _logger = logger;
        }

        public void Process()
        {
            using (_chromeDriver)
            using (_consumer)
            {
                _logger.Log(Microsoft.Extensions.Logging.LogLevel.Information, "Starting consumer.....");
                // Subscribe to the OnMessage event
                _consumer.OnMessage += (obj, msg) =>
                {
                    var endpoint = Constants.ImageUploadEndpoint;
                    _logger.Log(Microsoft.Extensions.Logging.LogLevel.Information, "Receiving message from kafka..");
                    var url = JsonConvert.DeserializeObject<UrlMessage>(msg.Value);
                    var json = string.Empty;
                    try
                    {
                        _chromeDriver.Navigate().GoToUrl(url.Url);
                        var screenshot = (_chromeDriver as ITakesScreenshot).GetScreenshot();
                        json = JsonConvert.SerializeObject(
                            new
                            {
                                url.Id,
                                Image = screenshot.AsByteArray
                            });
                    }

                    // catch the exception, write it to an error log
                    // on the file store
                    catch (Exception ex)
                    {
                        endpoint = Constants.TextFileUploadEndpoint;
                        json = JsonConvert.SerializeObject(
                            new
                            {
                                url.Id,
                                Text = $"Capturing {url.Url} Failed - Error: {ex.Message}"
                            });
                    }

                    ExecuteRequest(endpoint, json);
                };

                _consumer.Subscribe(
                    new List<string>()
                    {
                        _configuration.GetValue<string>(ConfigurationKeys.KafkaTopic)
                    });

                while (true)
                {
                    _consumer.Poll(100);
                }
            }
        }

        private void ExecuteRequest(string endpoint, string body)
        {
            var remoteHost = _configuration.GetValue<string>(ConfigurationKeys.UploadService);
            var client = new RestClient(remoteHost);
            var request = new RestRequest(endpoint, Method.POST);
            request.AddParameter("application/json; charset=utf-8", body, ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;

            var cancellationTokenSource = new CancellationTokenSource();
            var response = client.Execute(request);

            // log errors coming from dependent service
            if (!response.IsSuccessful)
            {
                _logger.Log(Microsoft.Extensions.Logging.LogLevel.Error, 
                    $"Error occured while attempting to request at " +
                    $"{remoteHost}.{endpoint}, error: {response.ErrorMessage}");
            }
        }
    }
}
