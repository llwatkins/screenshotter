﻿namespace UrlToScreenshotProcessor
{
    public static class ConfigurationKeys
    {
        public const string KafkaHost = nameof(KafkaHost);
        public const string KafkaPort = nameof(KafkaPort);
        public const string KafkaTopic = nameof(KafkaTopic);
        public const string KafkaConsumerGroup = nameof(KafkaConsumerGroup);
        public const string UploadService = nameof(UploadService);
    }
}
