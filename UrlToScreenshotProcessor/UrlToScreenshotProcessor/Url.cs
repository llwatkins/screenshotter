﻿using System;

namespace UrlToScreenshotProcessor
{
    public class UrlMessage
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
    }
}
