﻿using Confluent.Kafka;
using Confluent.Kafka.Serialization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UrlToScreenshotProcessor
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            await new HostBuilder()
                .ConfigureAppConfiguration((hostContext, configApp) =>
                {
                    configApp.SetBasePath(Directory.GetCurrentDirectory());
                    configApp.AddJsonFile("appsettings.json", optional: true);
                    configApp.AddEnvironmentVariables();
                })
                .ConfigureLogging((hostContext, configLogging) =>
                {
                    configLogging.AddConsole();
                })
                .ConfigureServices((hostContext, services) =>
                {
                    var options = new ChromeOptions();
                    options.AddArgument("--headless");
                    options.AddArgument("--whitelisted-ips");
                    options.AddArgument("--no-sandbox");
                    options.AddArgument("--disable-extensions");
                    var configuration = hostContext.Configuration;
                    services.AddLogging();

                    var kafkaHost = configuration.GetValue<string>(ConfigurationKeys.KafkaHost);
                    var kafkaPort = configuration.GetValue<string>(ConfigurationKeys.KafkaPort);
                    var kafkaTopic = configuration.GetValue<string>(ConfigurationKeys.KafkaTopic);
                    var consumerGroup = configuration.GetValue<string>(ConfigurationKeys.KafkaConsumerGroup);
                    services.AddHostedService<HostedService>();
                    services.AddSingleton(s 
                        => new EventConsumer(new Consumer<Null, string>(
                            new Dictionary<string, object>
                            {
                                { "group.id", consumerGroup },
                                { "bootstrap.servers", $"{kafkaHost}:{kafkaPort}" },
                            }, null, new StringDeserializer(Encoding.UTF8)),
                            new ChromeDriver(
                                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), 
                                options),
                            hostContext.Configuration,
                            services.BuildServiceProvider().GetService<ILogger<EventConsumer>>()
                        ));
                    services.BuildServiceProvider();
                })
                .UseConsoleLifetime()
                .Build()
                .RunAsync();
        }
    }
}
