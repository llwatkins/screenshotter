﻿using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;

namespace UrlToScreenshotProcessor
{
    public class HostedService : IHostedService
    {
        private readonly EventConsumer _eventConsumer;

        public HostedService(EventConsumer eventConsumer)
        {
            _eventConsumer = eventConsumer;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _eventConsumer.Process();

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken) 
            => Task.CompletedTask;
    }
}
