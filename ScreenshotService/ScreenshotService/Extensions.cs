﻿using System;

namespace ScreenshotService
{
    public static class Extensions
    {
        public static decimal Round(this decimal toRound)
            => Math.Round(toRound, 2, MidpointRounding.AwayFromZero);
    }
}
