﻿namespace ScreenshotService
{
    public static class EnvironmentVariables
    {
        public const string KafkaHost = nameof(KafkaHost);
        public const string KafkaTopic = nameof(KafkaTopic);
        public const string KafkaPort = nameof(KafkaPort);
        public const string RedisInstanceName = nameof(RedisInstanceName);
        public const string RedisConfiguration = nameof(RedisConfiguration);
        public const string AllowDuplicates = nameof(AllowDuplicates);
    }
}
