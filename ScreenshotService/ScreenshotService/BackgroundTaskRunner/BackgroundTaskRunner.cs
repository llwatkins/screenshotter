﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ScreenshotService
{
    public class BackgroundTaskRunner : BackgroundService
    {
        private readonly IBackgroundTaskQueue _backgroundTaskQueue;
        private readonly ILogger<BackgroundTaskRunner> _logger;

        public BackgroundTaskRunner(
            IBackgroundTaskQueue backgroundTaskQueue,
            ILogger<BackgroundTaskRunner> logger)
        {
            _backgroundTaskQueue = backgroundTaskQueue;
            _logger = logger;
        }

        // todo: what are the implications of this not
        // return a Task.Completed until work to do
        // when StartAsync called??
        protected async override Task ExecuteAsync(
            CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                var workItem = _backgroundTaskQueue.Dequeue(cancellationToken);

                try
                {
                    await workItem(cancellationToken);
                }
                catch (Exception ex)
                {
                    _logger.Log(LogLevel.Error, ex.Message);
                }
            }
        }
    }
}
