﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ScreenshotService
{
    public interface IBackgroundTaskQueue
    {
        void QueueBackgroundWorkItem(Func<CancellationToken, Task> workItem);

        Func<CancellationToken, Task> Dequeue(
            CancellationToken cancellationToken);
    }
}
