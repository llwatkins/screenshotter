﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace ScreenshotService
{
    public class BackgroundTaskQueue : IBackgroundTaskQueue
    {
        private BlockingCollection<Func<CancellationToken, Task>> _workItems =
            new BlockingCollection<Func<CancellationToken, Task>>();

        public void QueueBackgroundWorkItem(
            Func<CancellationToken, Task> workItem)
        {
            if (workItem == null)
            {
                throw new ArgumentNullException(nameof(workItem));
            }

            _workItems.Add(workItem);
        }

        /// <summary>
        /// Will block the calling thread while the collection is empty,
        /// forcing the caller to wait until there is work to do
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Func<CancellationToken, Task> Dequeue(
            CancellationToken cancellationToken)
            => _workItems.Take();
    }
}
