﻿using Microsoft.AspNetCore.Http;
using ScreenshotService.Core.Models;
using System.IO;
using System.Linq;

namespace ScreenshotService
{
    public static class DomainModelMapper
    {
        public static ScreenshotUrlsPackage ToScreenshotUrlsPackage(this IFormFile formFile, bool allowDuplicates)
        {
            var result = string.Empty;
            using (var reader = new StreamReader(formFile.OpenReadStream()))
            {
                result = reader.ReadToEnd();
            }

            var urls = result.Split(";").ToList();

            // remove EOF if exists
            if (string.IsNullOrEmpty(urls.Last()))
            {
                urls.RemoveAt(urls.Count - 1);
            }

            return new ScreenshotUrlsPackage(urls, allowDuplicates);
        }

        public static ScreenshotUrlsPackage ToScreenshotUrlsPackage(this string urls, bool allowDuplicates) 
            => new ScreenshotUrlsPackage(urls.Split(";").ToList(), allowDuplicates);
    }
}
