﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using ScreenshotService.Core.Models;
using ScreenshotService.Core.Services;

namespace ScreenshotService
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScreenshotsController : ControllerBase
    {
        private readonly IBackgroundTaskQueue _taskQueue;
        private readonly IScreenshotUrlsPackageDispatcher _dispatcher;
        private readonly IDistributedCache _distributedCache;
        private readonly IConfiguration _configuration;

        public ScreenshotsController(
            IBackgroundTaskQueue taskQueue,
            IScreenshotUrlsPackageDispatcher dispatcher,
            IDistributedCache distributedCache,
            IConfiguration configuration)
        {
            _taskQueue = taskQueue;
            _dispatcher = dispatcher;
            _distributedCache = distributedCache;
            _configuration = configuration;
        }

        [HttpGet("{id}/status")]
        public async Task<ActionResult<string>> GetStatus(Guid id)
        {
            var valueFromCache = await _distributedCache.GetStringAsync(id.ToString());
            if (valueFromCache == null)
            {
                return NotFound();
            }

            var cacheItem = valueFromCache.Deserialize();
            var percentComplete = (cacheItem.FilesProcessed / cacheItem.FilesToProcess) * 100;

            return $"{percentComplete.Round()}% of your screenshots have been uploaded!";
        }

        [HttpPost]
        public async Task<ActionResult<Guid>> Post([FromBody] string urls)
            => await SendUrlsAndGetId(() => urls.ToScreenshotUrlsPackage(
                _configuration.GetValue(EnvironmentVariables.AllowDuplicates, false)));

        [HttpPost("file")]
        public async Task<ActionResult<Guid>> PostFile([FromForm] IFormFile file)
            => await SendUrlsAndGetId(() => file.ToScreenshotUrlsPackage(
                _configuration.GetValue(EnvironmentVariables.AllowDuplicates, false)));

        private async Task<ActionResult<Guid>> SendUrlsAndGetId(Func<ScreenshotUrlsPackage> mappingFunc)
        {
            var package = mappingFunc();
            if (package.ValidationErrors.Any())
            {
                return BadRequest(package.ValidationErrors);
            }

            _taskQueue.QueueBackgroundWorkItem(async token =>
            {
                await _dispatcher.Send(package);
            });

            await Cache(package);

            return package.Id;
        }

        private async Task Cache(ScreenshotUrlsPackage package)
        {
            var cacheItem = new UploadStatusCacheItem(
                filesProcessed: 0, 
                filesToProcess: package.Urls.ToList().Count());

            // lets give it something high because without using more
            // kafka paritions, an un-processed set of urls could be waiting
            // for a while.
            var options = new DistributedCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromMinutes(20));

            await _distributedCache.SetStringAsync(
                package.Id.ToString(),
                cacheItem.Serialize(),
                options);
        }
    }
}
