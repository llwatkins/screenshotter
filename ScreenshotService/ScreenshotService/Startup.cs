﻿using System;
using System.Collections.Generic;
using System.Text;
using Confluent.Kafka;
using Confluent.Kafka.Serialization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ScreenshotService.Core.Services;
using ScreenshotService.Infrastructure;

namespace ScreenshotService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var kafkaHost = Configuration.GetValue<string>(EnvironmentVariables.KafkaHost);
            var kafkaPort = Configuration.GetValue<string>(EnvironmentVariables.KafkaPort);
            var kafkaTopic = Configuration.GetValue<string>(EnvironmentVariables.KafkaTopic);
            var redisInstanceName = Configuration.GetValue<string>(EnvironmentVariables.RedisInstanceName);
            var redisConfiguration = Configuration.GetValue<string>(EnvironmentVariables.RedisConfiguration);            
            services.AddSingleton<IEventProducer<string>, EventProducer>(s => new EventProducer(
                kafkaTopic,
                new Producer<Null, string>(new Dictionary<string, object>
                {
                    {
                        "bootstrap.servers",
                        $"{kafkaHost}:{kafkaPort}"
                    }
                }, null, new StringSerializer(Encoding.UTF8))));
            services.AddDistributedRedisCache(options =>
            {
                options.InstanceName = redisInstanceName;
                options.Configuration = redisConfiguration;
            });

            services.AddHostedService<BackgroundTaskRunner>();
            services.AddSingleton<IBackgroundTaskQueue, BackgroundTaskQueue>();
            services.AddSingleton<IScreenshotUrlsPackageDispatcher, ScreenshotUrlsPackageDispatcher>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
