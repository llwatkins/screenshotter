﻿using Newtonsoft.Json;

namespace ScreenshotService
{
    public static class CacheItemSerializer
    {
        public static UploadStatusCacheItem Deserialize(this string fromCache)
            // distributed cache apparently double-serializes the json so we need to get rid of the '\'
            => JsonConvert.DeserializeObject<UploadStatusCacheItem>(fromCache.Replace("\\", string.Empty));

        public static string Serialize(this UploadStatusCacheItem item)
            => JsonConvert.SerializeObject(item);
    }
}
