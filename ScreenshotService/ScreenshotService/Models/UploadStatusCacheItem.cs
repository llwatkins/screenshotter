﻿namespace ScreenshotService
{
    public class UploadStatusCacheItem
    {
        public UploadStatusCacheItem(
            decimal filesToProcess,
            decimal filesProcessed)
        {
            FilesToProcess = filesToProcess;
            FilesProcessed = filesProcessed;
        }

        public decimal FilesToProcess { get; }
        public decimal FilesProcessed { get; }
    }
}
