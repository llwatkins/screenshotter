﻿using Microsoft.AspNetCore.Http;
using NSubstitute;
using System;
using System.IO;

namespace ScreenshotService.Test
{
    public class TestBase
    {
        protected IFormFile GetIFormFile(string content)
        {
            var iFormFile = Substitute.For<IFormFile>();
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;

            var fileName = "test.txt";
            iFormFile.OpenReadStream().Returns(ms);
            iFormFile.FileName.Returns(fileName);
            iFormFile.ContentDisposition.Returns($"inline; filename={fileName}");

            return iFormFile;
        }
    }
}
