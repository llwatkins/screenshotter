﻿using FluentAssertions;
using System;
using System.IO;
using System.Linq;
using Xunit;

namespace ScreenshotService.Test
{
    public class DomainModelMapperTest : TestBase
    {
        [Fact(DisplayName = "It should read a form file and create a url package")]
        public void ItShouldReadAFormFileAndCreateModel()
        {
            var content = "https://www.detectify.com";
            var iFormFile = GetIFormFile(content);

            var result = iFormFile.ToScreenshotUrlsPackage(allowDuplicates: false);

            result.Id.Should().NotBe(default(Guid));
            result.Urls.Should().HaveCount(1);
            result.Urls.ToList().Single().Should().Be(content);
        }

        [Fact(DisplayName = "It should ignore EOF")]
        public void ItShouldNotAddEof()
        {
            var url = "https://www.detectify.com";
            var content = $"{url};";
            var iFormFile = GetIFormFile(content);

            var result = iFormFile.ToScreenshotUrlsPackage(allowDuplicates: false);

            result.Id.Should().NotBe(default(Guid));
            result.Urls.Should().HaveCount(1);
            result.Urls.ToList().Single().Should().Be(url);
        }
    }
}
