using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NSubstitute;
using ScreenshotService.Core.Services;
using ScreenshotService.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace ScreenshotService.Test
{
    public class ScreenshotsControllerTest : TestBase
    {
        private ScreenshotsController _subjectUnderTest;
        private BackgroundTaskRunner _backgroundTaskRunner;
        private TestDistributedCache _distributedCache;
        private IEventProducer<string> _eventProducer;
        private IBackgroundTaskQueue _backgroundTaskQueue;
        private IConfiguration _configuration;

        public ScreenshotsControllerTest()
        {
            _distributedCache = new TestDistributedCache();
            _eventProducer = Substitute.For<IEventProducer<string>>();
            _backgroundTaskQueue = new BackgroundTaskQueue();
            _backgroundTaskRunner = new BackgroundTaskRunner(
                _backgroundTaskQueue,
                Substitute.For<ILogger<BackgroundTaskRunner>>());
            _configuration = Substitute.For<IConfiguration>();

            _subjectUnderTest = new ScreenshotsController(
                _backgroundTaskQueue,
                new ScreenshotUrlsPackageDispatcher(_eventProducer),
                _distributedCache,
                _configuration);
        }

        private void MockAllowDuplicates(bool toReturn)
        {
            var section = Substitute.For<IConfigurationSection>();
            section.Value = toReturn.ToString();
            _configuration.GetSection(Arg.Any<string>()).Returns(section);
        }

        [Fact(DisplayName = "It should enqueue tasks and execute when uploading file")]
        public async Task ItShouldEnqueueTaskAndExecuteForFile()
        {
            MockAllowDuplicates(false);
            var content = "https://www.detectify.com;https://www.google.com;https://www.linkedin.com";
            var input = GetIFormFile(content);

            var id = (await _subjectUnderTest.PostFile(input)).Value;

            id.Should().NotBe(default(Guid));

            _eventProducer.Received(3);
        }

        [Fact(DisplayName = "It should return a bad request if file is empty")]
        public async Task ItShouldReturnABadRequestIfFileIsEmpty()
        {
            MockAllowDuplicates(false);
            var content = string.Empty;
            var input = GetIFormFile(content);

            var response = (await _subjectUnderTest.PostFile(input)).Result as ObjectResult;
            var values = response.Value as IEnumerable<string>;

            response.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            values.Should().HaveCount(1);
            values.Single().Should().Be("Urls cannot be empty.");
            _eventProducer.Received(0);
        }

        [Fact(DisplayName = "It should enqueue tasks and execute when passing string")]
        public async Task ItShouldEnqueueTaskAndExecuteForString()
        {
            MockAllowDuplicates(false);
            var input = "https://www.detectify.com;https://www.google.com;https://www.linkedin.com";

            var id = (await _subjectUnderTest.Post(input)).Value;

            id.Should().NotBe(default(Guid));

            _eventProducer.Received(3);
        }

        [Fact(DisplayName = "It should return a bad request if duplicate entries")]
        public async Task ItShouldReturnABadRequestIfDuplicateEntries()
        {
            // 1000 entries
            MockAllowDuplicates(false);
            var input = "https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com;https://www.google.com;https://www.detectify.com;https://www.linkedin.com;https://www.gmail.com";
            var response = (await _subjectUnderTest.Post(input)).Result as ObjectResult;
            var values = response.Value as IEnumerable<string>;

            response.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            values.Should().HaveCount(1);
            values.Single().Should().Be("You have duplicate entries in your url list.");
            _eventProducer.Received(0);
        }

        [Fact(DisplayName = "It should return a bad request if contains empty string")]
        public async Task ItShouldReturnABadRequestIfEmptyString()
        {
            MockAllowDuplicates(false);
            var input = string.Empty;
            var response = (await _subjectUnderTest.Post(input)).Result as ObjectResult;
            var values = response.Value as IEnumerable<string>;

            response.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            values.Should().HaveCount(1);
            values.Single().Should().Be("Cannot have empty urls. Ensure that the last entry is not followed by a ';'");
            _eventProducer.Received(0);
        }

        [Fact(DisplayName = "It should allow duplicates if AllowDuplicates is true")]
        public async Task ItShouldAllowDuplicatesIfAllowDuplicatesIsTrue()
        {
            MockAllowDuplicates(true);
            var input = "https://www.detectify.com;https://www.detectify.com";

            var id = (await _subjectUnderTest.Post(input)).Value;

            id.Should().NotBe(default(Guid));

            _eventProducer.Received(2);
        }

        [InlineData(96, 100, 96)]
        [InlineData(34, 296, 11.49)]
        [Theory(DisplayName = "It should return the percentage of files that have been processed")]
        public async Task ItShouldReturnThePercentageOfFilesProcessed(
            decimal processed,
            decimal toProcess,
            decimal expectedPercent)
        {
            _distributedCache.SetCacheItem(new UploadStatusCacheItem(toProcess, processed));
            var result = (await _subjectUnderTest.GetStatus(Guid.NewGuid())).Value;

            result.Should().Be($"{expectedPercent.ToString("0.00")}% of your screenshots have been uploaded!");
        }
    }
}
