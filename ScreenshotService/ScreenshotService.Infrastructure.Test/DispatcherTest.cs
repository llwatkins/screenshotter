using Newtonsoft.Json;
using NSubstitute;
using ScreenshotService.Core.Models;
using ScreenshotService.Core.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ScreenshotService.Infrastructure.Test
{
    public class DispatcherTest
    {
        private ScreenshotUrlsPackageDispatcher _subjectUnderTest;
        private IEventProducer<string> _eventProducer;

        public DispatcherTest()
        {
            _eventProducer = Substitute.For<IEventProducer<string>>();
            _subjectUnderTest = new ScreenshotUrlsPackageDispatcher(_eventProducer);
        }

        [Fact(DisplayName = "It should be called once for every url with correct id")]
        public async Task ItShouldBeCalledOnceForEveryUrl()
        {
            const string detectify = "https://www.detectify.com";
            const string google = "https://www.google.com";
            const string linkedIn = "https://www.linkedin.com";

            var package = new ScreenshotUrlsPackage(
                new List<string>()
                {
                    detectify,
                    google,
                    linkedIn
                });

            var first = JsonConvert.SerializeObject(new { package.Id, Url = detectify });
            var second = JsonConvert.SerializeObject(new { package.Id, Url = google });
            var third = JsonConvert.SerializeObject(new { package.Id, Url = linkedIn });

            await _subjectUnderTest.Send(package);

            _eventProducer.Received(3);
            await _eventProducer.Received(1).Produce(first);
            await _eventProducer.Received(1).Produce(second);
            await _eventProducer.Received(1).Produce(third);
        }
    }
}
