﻿using Confluent.Kafka;
using Newtonsoft.Json;
using ScreenshotService.Core.Models;
using ScreenshotService.Core.Services;
using System.Threading.Tasks;

namespace ScreenshotService.Infrastructure
{
    public class ScreenshotUrlsPackageDispatcher : IScreenshotUrlsPackageDispatcher
    {
        private readonly IEventProducer<string> _eventProducer;

        public ScreenshotUrlsPackageDispatcher(
            IEventProducer<string> eventProducer)
        {
            _eventProducer = eventProducer;
        }

        /// <summary>
        /// Produce each url to kafka
        /// </summary>
        /// <param name="package" cref=ScreenshotUrlsPackage>Contains an id pertaining to the request 
        /// and the list of urls</param>
        public async Task Send(ScreenshotUrlsPackage package)
        {
            foreach (var url in package.Urls)
            {
                await _eventProducer.Produce(
                    JsonConvert.SerializeObject(
                        new
                        {
                            package.Id,
                            Url = url
                        }));
            }
        }
    }
}
