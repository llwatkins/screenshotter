﻿using Confluent.Kafka;
using ScreenshotService.Core.Services;
using System.Threading.Tasks;

namespace ScreenshotService.Infrastructure
{
    public class EventProducer : IEventProducer<string>
    {
        private readonly string _kafkaTopic;
        private readonly Producer<Null, string> _producer;

        public EventProducer(
            string kafkaTopic,
            Producer<Null, string> producer)
        {
            _kafkaTopic = kafkaTopic;
            _producer = producer;
        }

        public async Task Produce(string t)
            => await _producer.ProduceAsync(_kafkaTopic, null, t);
    }
}
