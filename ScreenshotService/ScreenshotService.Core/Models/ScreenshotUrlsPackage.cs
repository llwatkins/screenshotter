﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ScreenshotService.Core.Models
{
    public class ScreenshotUrlsPackage
    {
        public ScreenshotUrlsPackage(
            List<string> urls,
            bool allowDuplicates = false)
        {
            Id = Guid.NewGuid();
            Urls = urls.ToHashSet();
            Validate(urls, allowDuplicates);
        }

        public Guid Id { get; }

        // Use a hash set which will give us unique urls.
        public HashSet<string> Urls { get; }
        public IEnumerable<string> ValidationErrors { get; private set; } = new List<string>();

        private void Validate(List<string> urls, bool allowDuplicates)
        {
            var validationErrors = ValidationErrors.ToList();
            if (!Urls.Any())
            {
                validationErrors.Add("Urls cannot be empty.");
            }

            // Make this a configure-able validation
            if (!allowDuplicates && urls.Count() > Urls.Count())
            {
                validationErrors.Add("You have duplicate entries in your url list.");
            }

            // lets use contains since we're using a hash set
            if (Urls.Contains(string.Empty))
            {
                validationErrors.Add(
                        "Cannot have empty urls. Ensure that the last entry is not followed by a ';'");
            }

            ValidationErrors = validationErrors;
        }
    }
}
