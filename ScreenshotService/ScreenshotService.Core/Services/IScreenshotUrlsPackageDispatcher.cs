﻿using ScreenshotService.Core.Models;
using System.Threading.Tasks;

namespace ScreenshotService.Core.Services
{
    public interface IScreenshotUrlsPackageDispatcher
    {
        Task Send(ScreenshotUrlsPackage package);
    }
}
