﻿using System.Threading.Tasks;

namespace ScreenshotService.Core.Services
{
    /// <summary>
    /// Producer interface
    /// </summary>
    /// <typeparam name="T">the type that is being produced</typeparam>
    public interface IEventProducer<T>
    {
        Task Produce(T t);
    }
}
