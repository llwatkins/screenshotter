using FluentAssertions;
using ScreenshotService.Core.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace ScreenshotService.Core.Test
{
    public class ScreenshotUrlsPackageTest
    {
        private ScreenshotUrlsPackage _subjectUnderTest;

        [Fact(DisplayName = "It should set id during instantiation")]
        public void ItShouldSetId()
        {
            _subjectUnderTest = new ScreenshotUrlsPackage(new List<string>());
            _subjectUnderTest.Id.Should().NotBe(default(Guid));
        }

        [Fact(DisplayName = "It should set urls")]
        public void ItShouldSetUrls()
        {
            var urls = new List<string>
            {
                "https://www.google.com"
            };

            _subjectUnderTest = new ScreenshotUrlsPackage(urls);
            _subjectUnderTest.Id.Should().NotBe(default(Guid));
            _subjectUnderTest.Urls.Should().HaveCount(1);
        }
    }
}
